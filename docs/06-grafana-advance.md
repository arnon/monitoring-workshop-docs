# Grafana Advanced Workshop

## Grafana Variables

* Go to `Dashboard settings` and `Variables` tab
* Click on `Add variable`
  * Name: `instance`
  * Label: `Instance`
  * Query type: `Series query`
  * Query: `label_values(node_uname_info{job="node-exporter"}, instance)`
* `Apply`
* Edit `Load Average`
* Add `Label filters` on each query
  * `instance = $instance`

## Exercise Grafana Variables

* Change `Memory Usage` panel to use `$instance` variable
* Don't forget to save your dashboard

## Grafana Gauge

* Add a new `Visualization`
* Change visualization to `Gauge`
* Title: `Memory Usage`
* Change query to `Code` and put the follow

```promql
100 -
(
  avg(node_memory_MemAvailable_bytes{job="node-exporter", instance="$instance"}) /
  avg(node_memory_MemTotal_bytes{job="node-exporter", instance="$instance"})
* 100
)
```

* Unit: `Percent (0-100)`
* Min: `0`
* Max: `100`
* Thresholds
  * `80: Orange`
  * `90: Red`
* Save dashboard

## Grafana Row

* `Add` > `Add a new row`
* Edit `Row title` to `CPU`
* Add new row to `Memory`
* Arrange each visualization to each row
* Save dashboard

## Grafana Table

* Add a new visualization
* Change visualization to `Table`
* Title: `Disk Space Usage`
* Change query to `Code` and put `max by (mountpoint) (node_filesystem_size_bytes{job="node-exporter", instance="$instance"})`
  * Options
    * Format: `Table`
    * Type: `Instant`
* Duplicate another query with `max by (mountpoint) (node_filesystem_avail_bytes{job="node-exporter", instance="$instance"})`
* Settings
  * Unit: `bytes(SI)`
* Go to tab `Transform`
* Choose `Group by`
  * mountpoint: `Group by`
  * Value #A: `Calculate` with `Last *`
  * Value #B: `Calculate` with `Last *`
* Add transformation: `Merge`
* Add transformation: `Add field from calculation`
  * Mode: `Binary operation`
  * Operation: `Value #A (lastNotNull)` `-` `Value #B (lastNotNull)`
  * Alias: `Used`
* Add transformation: `Add field from calculation`
  * Mode: `Binary operation`
  * Operation: `Used` `/` `Value #A (lastNotNull)`
  * Alias: `Used, %`
* Add transformation: `Organize fields`
  * mountpoint: `Mounted on`
  * Value #A (lastnotNull): `Size`
  * Value #B (lastnotNull): `Available`
* Add transformation: `Sort by`
  * Field: `Mounted on`
* Go to `Overrides` Tab
  * Fields with name: `Used, %`
    * Unit: `Percent (0.0-1.0)`
    * Cell Type: `Gauge`
      * Gauge Display Mode: `Gradient`
    * Max: `1`
    * Min: `0`
  * Fields with name: `Mounted on`
    * Column width: `220`
  * Adjust width on each field
* Add `Row` Disk and move the Disk Space Usage panel under it

## Exercise Grafana

* Create `Disk I/O` Visualization with the following data
  * Legend `{{device}} read` as `rate(node_disk_read_bytes_total{job="node-exporter", instance="$instance"}[$__rate_interval])`
  * Legend `{{device}} write` as `rate(node_disk_written_bytes_total{job="node-exporter", instance="$instance"}[$__rate_interval])`
  * Legend `{{device}} io time` as `rate(node_disk_io_time_seconds_total{job="node-exporter", instance="$instance"}[$__rate_interval])`
  * Title: `Disk I/O`
  * Tooltip mode: `All`
  * Unit: `bytes(IEC)`
  * Overrides
    * Fields with name matching regex: `/ io time/`
    * Unit: `Percent (0.0-1.0)`
* Create `CPU Usage` Visualization with the following data

```promql
(
  (1 - sum without (mode) (rate(node_cpu_seconds_total{job="node-exporter", mode=~"idle|iowait|steal", instance="$instance"}[$__rate_interval])))
/ ignoring(cpu) group_left
  count without (cpu, mode) (node_cpu_seconds_total{job="node-exporter", mode="idle", instance="$instance"})
)
```

  * Legends: `{{cpu}}`
  * Title: `CPU Usage`
  * Tooltip mode: `All`
  * Fill opacity: `10`
  * Stack series: `Normal`
  * Unit: `Percent(0.0-1.0)`
  * Min: `0`
  * Max: `1`
  * Apply
* Arrange each `panel` to own row
* Save dashboard


## Import Node Exporter Full Dashboard

* Go to <https://grafana.com/grafana/dashboards/> and search for `node exporter`
* Choose `Node Exporter Full`
* Click on `Copy ID to clipboard`
* Back to Grafana Dashboard page
* Click on `New` > `Import`
* Paste copied ID to `Import via grafana.com` then click `Load`
* Choose `Prometheus` as data source and click `Import`
* Walk through dashboard


## Navigation
* Previous: [Grafana Basic](05-grafana-basic.md)
* [Home](../README.md)
* Next: [Grafana Provision](07-grafana-provision.md)
