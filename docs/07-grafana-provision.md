# Grafana Provisioning Workshop

## Create Grafana Provisioning files

* `touch ~/monitoring-workshop/workshop/grafana/config/provisioning/dashboards/my-dashboard.yml`

```yaml
apiVersion: 1
providers:
- name: 'my-dashboard'
  orgId: 1
  type: 'file'
  disableDeletion: true
  editable: true
  options:
    path: '/etc/grafana/provisioning/dashboards/json/'
    foldersFromFilesStructure: true
```

* `mkdir -p workshop/grafana/config/provisioning/dashboards/json/training/` to create dashboard files directory
* Go to `Google Cloud Shell Exporter` dashboard and click on `Share dashboard or panel` button at the top
  * Go to tab `Export`
  * Checked `Export for sharing externally` and click on `View JSON`
  * Copy JSON and put on `workshop/grafana/config/provisioning/dashboards/json/training/node-exporter-basic.json`
  * Search for `${DS_PROMETHEUS}` and replace with `prometheus`
* Do the same to `Node Exporter Full` dashboard with filename `node-exporter-full.json`
* Run the following commands to use Grafana provisioning

```bash
docker restart grafana
```

* Check all your dashboards
* Edit `workshop/grafana/config/provisioning/dashboards/json/training/node-exporter-basic.json` title at the last line to `Node Exporter Basic`. Save and see the Grafana for change

## Exercise 1

* From Nginx Exporter. Provision Nginx Exporter Dashboard from <https://github.com/nginxinc/nginx-prometheus-exporter/blob/main/grafana/dashboard.json>

## Exercise 2 Monitoring Docker

* Add cAvisor container with the following in `workshop/docker-compose.yml` file

```yaml
  cadvisor:
    image: gcr.io/cadvisor/cadvisor:v0.47.2
    container_name: cadvisor
    privileged: true
    ports:
      - 8888:8080
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:rw
      - /sys:/sys:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
    networks:
      - monitoring
```

* Add Prometheus job to read cAdvisor metrics from `http://cadvisor:8080/metrics`
```yaml
- job_name: "cadvisor"
    # metrics_path defaults to '/metrics'
    # scheme defaults to 'http'.
    static_configs:
      - targets: ["cadvisor:8080"]
```
* Run `curl --request POST http://localhost:9090/-/reload`
* Check `Status` > `Targets` on Prometheus
* Find the `cAdvisor exporter` dashboard in Grafana Dashboard <https://grafana.com/grafana/dashboards> and do Grafana provision

## Navigation
* Previous: [Grafana Advance](06-grafana-advance.md)
* [Home](../README.md)
* Next: [Grafana Loki](08-grafana-loki.md)
