# Prometheus Alertmanager
## Update file
* patse file `workshop/alertmanager/alertmanager.yml`

```yml
route:
  # When a new group of alerts is created by an incoming alert, wait at
  # least 'group_wait' to send the initial notification.
  # This way ensures that you get multiple alerts for the same group that start
  # firing shortly after another are batched together on the first
  # notification.
  group_wait: 10s

  # When the first notification was sent, wait 'group_interval' to send a batch
  # of new alerts that started firing for that group.
  group_interval: 30s

  # If an alert has successfully been sent, wait 'repeat_interval' to
  # resend them.
  repeat_interval: 30m

  # A default receiver
  receiver: 'webhook.default'

receivers:
  - name: 'webhook.default'
    webhook_configs:
      - url: 'https://webhook.site/3d706e5e-80de-4f51-9512-66f9e3628e30'
        send_resolved: true
```

* save file
* run command `docker compose up -d` 

## Create first alert

* Add the following to `workshop/prometheus/config/rules/alerts_rules.yaml`
```yaml
groups:
- name: my-alerts
  rules:
  - alert: NodeExporterDown
    expr: up{job="node-exporter"} == 0
```

* Run `curl --request POST http://localhost:9090/-/reload` to reload Prometheus configuration
* Go to `Status` > `Rules` to see your alert rule
* Go to `Alerts` menu to see status of your alert
* Run `docker stop node-exporter` to stop Node Exporter
* Wait for 15 seconds and see the `Alerts` again
* Run `docker start node-exporter` to start Node Exporter, wait for 15 seconds and check the `Alerts` again

## Create alert firing wait

* The `for` clause causes Prometheus to wait for a certain duration. Prometheus will check that the alert continues to be active during each evaluation for 1 minutes before firing the alert. Elements that are active, but not firing yet, are in the pending state.
* Replace `workshop/prometheus/config/rules/alerts_rules.yaml` with the following

```yaml
groups:
- name: my-alerts
  rules:
  - alert: NodeExporterDown1Min
    expr: up{job="node-exporter"} == 0
    for: 1m
```

* Run `curl --request POST http://localhost:9090/-/reload` to reload Prometheus configuration
* Run `docker stop node-exporter` to stop Node Exporter and wait for 15 seconds to a minute to see the status `Alerts` from `PENDING` to `FIRING`
* Run `docker start node-exporter` to start Node Exporter, wait for 15 seconds and check the `Alerts` again

## Create alert with label

* Replace `workshop/prometheus/config/rules/alerts_rules.yaml` with the following

```yaml
groups:
- name: my-alerts
  rules:
  - alert: NodeExporterDown30Secs
    expr: up{job="node-exporter"} == 0
    for: 30s
    labels:
      service: node-exporter
      severity: warning
  - alert: NodeExporterDown1Min
    expr: up{job="node-exporter"} == 0
    for: 1m
    labels:
      service: node-exporter
      severity: critical
```

* Run `curl --request POST http://localhost:9090/-/reload` to reload Prometheus configuration
* Go to <https://webhook.site> and copy the URL
* Replace `workshop/alertmanager/alertmanager.yaml` with the following

```yaml
route:
  # When a new group of alerts is created by an incoming alert, wait at
  # least 'group_wait' to send the initial notification.
  # This way ensures that you get multiple alerts for the same group that start
  # firing shortly after another are batched together on the first
  # notification.
  group_wait: 10s

  # When the first notification was sent, wait 'group_interval' to send a batch
  # of new alerts that started firing for that group.
  group_interval: 30s

  # If an alert has successfully been sent, wait 'repeat_interval' to
  # resend them.
  repeat_interval: 30m

  # A default receiver
  receiver: 'webhook.default'

  # All the above attributes are inherited by all child routes and can
  # overwritten on each.
  routes:
  - receiver: 'webhook.warning'
    matchers:
    - service="node-exporter"
    - severity="warning"
  - receiver: 'webhook.critical'
    matchers:
    - service="node-exporter"
    - severity="critical"

receivers:
  - name: 'webhook.default'
    webhook_configs:
      - url: 'https://webhook.site/CHANGEME'
        send_resolved: true
  - name: 'webhook.warning'
    webhook_configs:
      - url: 'https://webhook.site/CHANGEME'
        send_resolved: true
  - name: 'webhook.critical'
    webhook_configs:
      - url: 'https://webhook.site/CHANGEME'
        send_resolved: true
```

* Run `curl --request POST http://localhost:9093/-/reload` to reload Alertmanager configuration
* Web preview to port `9093` to see the status of alert in Alertmanager
* Run `docker stop node-exporter` to stop Node Exporter to firing alerts
* Wait for alert and check `workshop/webhook/logs/` directory to see alert firing via webhook or check on your <https://webhook.site>
* (Option)Delete alertmanager data and restart alertmanager in case of you are you not seeing hook file
  * `cd workshop/`
  * `rm -rf alertmanager/data/* && docker compose restart alertmanager`
* Run `docker start node-exporter` to see the hook change

## Create alert with annotation for more information

* Replace `workshop/prometheus/config/rules/alerts_rules.yaml` with the following

```yaml
groups:
- name: my-alerts
  rules:
  - alert: NodeExporterDown30Secs
    expr: up{job="node-exporter"} == 0
    for: 30s
    labels:
      service: node-exporter
      severity: warning
    annotations:
      description: '{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 30 seconds.'
      summary: 'Warning: Instance {{ $labels.instance }} down'
  - alert: NodeExporterDown1Min
    expr: up{job="node-exporter"} == 0
    for: 1m
    labels:
      service: node-exporter
      severity: critical
    annotations:
      description: '{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 1 minute.'
      summary: 'Critical: Instance {{ $labels.instance }} down'
```

* Run `curl --request POST http://localhost:9090/-/reload` to reload Prometheus configuration
* Run `docker stop node-exporter` to stop Node Exporter to firing alerts
* Go to `Alerts` menu to see status of your alert
* Go to Alertmanager and click on `Info` to see annotations
* Run `docker start node-exporter` get get service back

## Create alert with active time and mute time

* Replace `workshop/alertmanager/alertmanager.yaml` with the following

```yaml
time_intervals:
  - name: weekdays
    time_intervals:
    - times:
      weekdays: ['monday:friday']
      location: 'Asia/Bangkok'
  - name: weekends
    time_intervals:
    - times:
      weekdays: ['sunday', 'saturday']
      location: 'Asia/Bangkok'

route:
  # When a new group of alerts is created by an incoming alert, wait at
  # least 'group_wait' to send the initial notification.
  # This way ensures that you get multiple alerts for the same group that start
  # firing shortly after another are batched together on the first
  # notification.
  group_wait: 10s

  # When the first notification was sent, wait 'group_interval' to send a batch
  # of new alerts that started firing for that group.
  group_interval: 30s

  # If an alert has successfully been sent, wait 'repeat_interval' to
  # resend them.
  repeat_interval: 30m

  # A default receiver
  receiver: 'webhook.default'

  # All the above attributes are inherited by all child routes and can
  # overwritten on each.
  routes:
  - receiver: 'webhook.warning'
    matchers:
    - service="node-exporter"
    - severity="warning"
    mute_time_intervals:
    - weekdays
  - receiver: 'webhook.critical'
    matchers:
    - service="node-exporter"
    - severity="critical"
    active_time_intervals:
    - weekdays
    - weekends

...
```

* Run `curl --request POST http://localhost:9093/-/reload` to reload Alertmanager configuration
* Run `docker stop node-exporter` to stop Node Exporter to firing alerts
* You will see that `NodeExporterDown30Secs` alert will be muted
* Change `mute_time_intervals` to `weekends` and reload Alertmanagaer configuration
* Run `docker start node-exporter` get get service back

## Create suppress alert

* Append `workshop/alertmanager/alertmanager.yaml` with the following

```yaml
inhibit_rules:
  - equal: [instance]
    source_match:
      severity: 'critical'
    target_match:
      severity: 'warning'
```

* Reload Alertmanager and stop node-exporter container to see `warning` alert has been suppressed when critical alert firing
* Comment or remove `inhibit_rules`, reload service, and start node-exporter container again

## Alert Exercise

Write alert name `mod-alerts` with the following rules

* if instance `app-demo-1:8080` is down send to webhook 
  * `warning` alert should send on `office-hours` that is `monday` to `friday` at `9.00 - 17.00` only
  * severity will be warning when the alert is running for 15 seconds
severity will be critical when the alert is running for 30 seconds
* Hint: use the following `time_intervals` as `office-hours`
```yaml
  - name: office-hours
    time_intervals:
    - times:
      - start_time: "09:00"
        end_time: "17:00"
      weekdays: ['monday:friday']
      location: 'Asia/Bangkok'
```


## Navigation
* Previous: [Prometheus Basic Workshop](02-prom-basic.md)
* [Home](../README.md)
* Next: [Prometheus Exporter](04-prom-exporter.md)
