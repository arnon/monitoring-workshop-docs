# Grafana Basic Workshop

## Add Prometheus data source

* Web preview port 9000
  * Username: `admin`
  * Password: `admin`
  * Put new password: `password`
* Go to `Connections` > `Data sources` menu on the left show `Prometheus` select and click `Test` button
* Click on `Explore view` link

## Add first query

* On query `A`
  * Explain: `checked`
  * Metric: `node_load1`
  * Label filters: `job = node-exporter`
  * Options
    * Legend: `Verbose`
    * Type: `Range`
* `Run query` on the top right
* `Add query`
* On query `B`
  * Metric: `node_load5`
  * Label filters: `job = node-exporter`
  * Options
    * Legend: `Verbose`
    * Type: `Range`
* Use `Duplicate query`
* On query `C`
  * Metric: `node_load15`
* Graph
  * Test between `Lines`, `Bars`, `Points`, `Stacked lines`, `Stacked bars`
  * Pick `Lines`
* Click `Add to dashboard` at the top right
  * Choose `New dashboard`
  * Click on `Open dashboard`
  * Click on `Save dashboard` button at the top right
  * Dashboard name: `Google Cloud Shell Node Exporter`
  * Click on `Save`

## Play with Grafana Dashboard

* `Dashboard` menu on the left to see your newly created dashboard
* Click on `Dashboard settings` button at the top right
* See and check on each settings
* Click on `New Panel` menu on the right > `Edit`
  * Panel options
    * Title: Load Average
  * Tooltip
    * Try every `Tooltip mode` and Choose `All`
  * Try all `Legend` options but leave everything default
  * Axis
    * Try all `Placement` options
    * Label: try to put `Load Number`
    * Try other options
  * Try all `Graph styles` but leave everything default
  * Try all `Standard options` but leave everything default
  * Add `Query`
    * Metric: `node_cpu_seconds_total`
    * Label filters
      * `job = node-exporter`
      * `mode = idle`
    * Operations
      * `Aggregations` > `Count`
* `Apply`
* Change time range to `Last 1 hour`
* `Save dashboard` with `Save current time range as dashboard default` checked

## Override (change Display name)

* Edit `Load Average` panel again
* Click on `Overrides` tab at the right, then `Add field override`, and choose `Fields with name`
  * Choose `node_load1{...}`
  * Click on `Add override property` and search for `Display name`
  * Change name to `1m load average`
* Do the other queries with the following legends
  * `5m load average`
  * `15m load average`
  * `logical core(s)`
* `Save dashboard`

## Exercise Memory Usage

* Create `Memory Usage` Panel with the following custom legends and metrics
  * `memory buffers` from `node_memory_Buffers_bytes` `job="node-exporter"`
  * `memory cached` from `node_memory_Cached_bytes` `job="node-exporter"`
  * `memory free` from `node_memory_MemFree_bytes` `job="node-exporter"`
  * `memory usage` by `node_memory_MemTotal_bytes` `job="node-exporter"` minus memory free, buffers, and cached
* Use `Stack series` with `Normal` so you will see total memory stacked
* `memory usage` will be the first stack.  
  hint ` move query to first query`
* `Fill opacity` to `10`
* Change `Unit` to `bytes(IEC)` that will divide by 1024
* `Tooltip mode` to `All`
* Save Dashboard

## Navigation
* Previous: [Prometheus Exporter](04-prom-exporter.md)
* [Home](../README.md)
* Next: [Grafana Advance](06-grafana-advance.md)
