# Prometheus Basic Workshop
## Walk through Prometheus
* Web Preview Port 9090
* Check each page on `Status` menu
* Check these values in `Status` > `Configuration`

```yaml
global:
  scrape_interval: 15s # How frequently to scrape targets by default.
  scrape_timeout: 10s # How long until a scrape request times out.
  evaluation_interval: 15s # How frequently to evaluate rules.
scrape_configs:
- job_name: prometheus # The job name assigned to scraped metrics by default.
  metrics_path: /metrics # The HTTP resource path on which to fetch metrics from targets.
  scheme: http
  static_configs: # List of labeled statically configured targets for this job.
  - targets:
    - localhost:9090 # Scrape yourself Prometheus metrics
- job_name: "grafana"
  scheme: http
  static_configs:
    - targets: ["grafana:3000"]
```

* Check `Status` > `Targets` and try to access Web Preview with `/metrics` as path

## Play with Graph
* Add image to docker-compose.yml
```yaml
  node-exporter:
    container_name: node-exporter
    image: prom/node-exporter:v1.6.1
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    command:
      - '--path.procfs=/host/proc'
      - '--path.rootfs=/rootfs'
      - '--path.sysfs=/host/sys'
      - '--collector.filesystem.mount-points-exclude=^/(sys|proc|dev|host|etc)($$|/)'
    ports:
      - 9100:9100
    networks:
      - monitoring

  app-demo-1:
    container_name: app-demo-1
    image: julius/prometheus-demo-service:0.11.1
    ports:
      - 10001:8080
    networks:
      - monitoring
  app-demo-2:
    container_name: app-demo-2
    image: julius/prometheus-demo-service:0.11.1
    ports:
      - 10002:8080
    networks:
      - monitoring
```
* run command
```bash
docker compose up -d
```
* Check `Use local time`
* Click on `Open metrics explorer` button
* Click on `up` then `Execute` and see value
* Click on `Graph` tab
* Play with filter and range
* Try to `Add Panel`
* Query to `scrape_duration_seconds` and `prometheus_http_requests_total` on new panels. Observe what is data type for each metric.


* Append the following to workshop/prometheus/config/prometheus.yml
```yaml
  - job_name: "node-exporter"
    static_configs:
      - targets: ["node-exporter:9100"]

  - job_name: "app-demo"
    static_configs:
      - targets: ["app-demo-1:8080"]
      - targets: ["app-demo-2:8080"]
```

* Run `curl --request POST http://localhost:9090/-/reload`
* Check `Status` > `Targets` on Prometheus
* click Graph
* Test by `node_disk_info` and Execute


## Selectors and Matchers

### [Instant Vector Selectors](https://prometheus.io/docs/prometheus/latest/querying/basics/#instant-vector-selectors)

* `prometheus_http_requests_total{handler='/graph'}`
* `prometheus_http_requests_total{handler=~'/api/v1/.*'}`
* `prometheus_http_requests_total{handler!~'/api/v1/.*'}`
* `prometheus_http_requests_total{handler=~'/graph|/metrics'}`
* `prometheus_http_requests_total{code=~'2.*|3.*'}`
* What is the query to select all HTTP status codes except 4xx ones?
    - [Hint](https://prometheus.io/docs/prometheus/latest/querying/examples/)
* What is the query to check `demo_api_request_duration_seconds_count` to query job `app-demo` on instance `app-demo-1:8080` with method that is NOT `POST` and status `200` on path `/api/bar` or `/api/foo`?


### [Range Vector Selectors](https://prometheus.io/docs/prometheus/latest/querying/basics/#range-vector-selectors)
* The following queries can not work in `Graph` tab
* `prometheus_http_requests_total[1m]`
* `prometheus_http_requests_total[2m]`
* `prometheus_http_requests_total{handler='/'}[2m]`
* What is the query to select 2xx HTTP status codes on /graph page in one hour?
* Hint: Use `,`
* What is the query to check `demo_memory_usage_bytes` for `free` memory for all apps in last 5 minutes?


## [Arithmetic Binary Operators](https://prometheus.io/docs/prometheus/latest/querying/operators/#arithmetic-binary-operators)

* Test `node_filesystem_size_bytes`
  * `node_filesystem_size_bytes`
  * `node_filesystem_size_bytes/1024/1024` หน่วย MB
  * `node_filesystem_size_bytes/1024/1024/1024`  หน่วย GB
  * `node_filesystem_size_bytes / 1024^3` หน่วย GB
* `demo_disk_usage_bytes / demo_disk_total_bytes * 100` เปอร์เซ็นต์ของการใช้งานดิสก์

## [Comparison Binary Operators](https://prometheus.io/docs/prometheus/latest/querying/operators/#comparison-binary-operators)

* `node_cpu_seconds_total`
  * `node_cpu_seconds_total>300`
  * `node_cpu_seconds_total==0`


## [Counter Rates and Increases](https://prometheus.io/docs/prometheus/latest/querying/functions)

* `demo_api_request_duration_seconds_count`
* `demo_api_request_duration_seconds_count[5m]`
* The per-second increase of all the `demo_api_request_duration_seconds_count` series as averaged over a five-minute window

  Rate [ref](https://prometheus.io/docs/prometheus/latest/querying/functions/#rate)
  * `rate(demo_api_request_duration_seconds_count[5m])`
  * Try to reduce range size to 1 minute
* Instead of averaging over the entire provided time window, `irate` [ref](https://prometheus.io/docs/prometheus/latest/querying/functions/#irate) only considers the last two points under the provided window and calculates an instantaneous rate from them
  * `irate(demo_api_request_duration_seconds_count[5m])`
  * Try to reduce range size to 1 minute
* The `demo_cpu_usage_seconds_total` metric counts the number of seconds spent in different CPU modes since process start time. Build a query that calculates the CPU usage in number of cores, as averaged over the last 10 minutes.



## Aggregation Operators

* `rate(demo_api_request_duration_seconds_count[5m])`
* `sum(rate(demo_api_request_duration_seconds_count[5m]))`
* `sum by(path, method, status) (rate(demo_api_request_duration_seconds_count[5m]))`
* `avg by(path, method, status) (rate(demo_api_request_duration_seconds_count[5m]))`
* `group by(mode) (demo_cpu_usage_seconds_total)`
* `count(group by(mode) (demo_cpu_usage_seconds_total))`
* Build a query that calculates the sum total memory usage (metric `process_resident_memory_bytes`) of all `app-demo` job in all instances
* Build a query that calculates the sum total memory usage (metric `process_resident_memory_bytes`) split out by `job`


## Navigation
* Previous: [Prepare Environment](01-prepare.md)
* [Home](../README.md)
* Next: [Prometheus Alertmanager](03-prom-alert.md)
