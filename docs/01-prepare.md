# Monitoring Workshop

- Grafana
- Prometheus
- Loki
- Tempo

Docker compose

```bash
cd ~/monitoring-workshop-project/workshop
docker compose up -d
docker ps -a
docker logs grafana
```

# Error prometheus(Option)

- prometheus permission
  ```bash
  sudo chown -R 1000:1000 prometheus/
  sudo chmod -R 775 prometheus/
  ```

#  Error Grafana cors env WEB_HOST miss(Option)
* check web url
```bash
echo ${WEB_HOST}
```
* Editor -> web preview -> input port Ex:9090 -> new window url 

https://9090-cs-cf294812-38bf-4776-a9c4-6b047c3d998b.cs-asia-southeast1-yelo.cloudshell.dev

https://9090-<URL_HOST>

export WEB_HOST=cs-cf294812-38bf-4776-a9c4-6b047c3d998b.cs-asia-southeast1-yelo.cloudshell.dev
```bash
docker compose down grafana nginx

export WEB_HOST=<URL_HOST>

docker compose up -d  grafana nginx
```
```bash
export WEB_HOST=<URL_HOST>
```


## Navigation

* [Home](../README.md)
* Next: [Prometheus Basic Workshop](02-prom-basic.md)
