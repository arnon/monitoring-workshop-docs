# Grafana Tempo

### Create Datasource
* Click toggle menu -> Conections -> Add new connection
* Search `tempo` and click select `Tempo` -> Add new data source
* http -> URL Input -> `http://tempo:3200`
* Click `Save & test`
*

### Filter Data with Builder
* Click toggle menu -> Explore -> Tops Datasource -> select `Tempo`
* Query A
* Resource Service Name `=` `basic-service` 
* click `Run query`<br /><br />

* Duration > `10s`
* click `Run query`<br /><br />

* copy Trace ID to paste tab `TraceQL`

### Filter Data with Code
* click tab TraceQL
* `{.service.name="basic-service" && duration>10}`


### Filter On MOD DEV JSON
* Query A -> click `Code`
* `{.service.name="order-management-service"}`
* `{.service.name="order-management-service" && .http.url="http://dev-mod-om-gateway.myorder.dev/order-management/complete/filter"}`
* `{.service.name="order-management-service" && .http.url="http://dev-mod-om-gateway.myorder.dev/order-management/complete/filter" && .http.method="POST"}`
* `{.service.name="order-management-service" && .http.url="http://dev-mod-om-gateway.myorder.dev/order-management/complete/filter" && .http.method="POST" && duration > 1s}`

* `{.service.name="order-management-service" && status=error  }`


## Navigation
* Previous: [Grafana Loki](08-grafana-loki.md)
* [Home](../README.md)
