# Prometheus Blackbox Exporter Workshop

## HTTP Probes

* Append the following to `workshop/prometheus/config/prometheus.yml`

```yaml
  - job_name: 'blackbox-http'
    metrics_path: /probe # Don't scrape /metrics, but /probe on the Blackbox Exporter.
    params:
      module:
        - http_2xx # Select the "http_2xx" module configured in the Blackbox Exporter.
    static_configs:
      - targets:
        - 'http://myorder.ai'     # Target to probe with HTTP.
        - 'https://myorder.ai'    # Target to probe with HTTPS.
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__address__]
        target_label: instance
      - target_label: __address__
        replacement: blackbox-exporter:9115 # Blackbox exporter address.
```
* Run curl --request POST http://localhost:9090/-/reload to reload Prometheus configuration
* check `Status` > `Targets`
* `UP` doesn't mean it test succeed
* HTTP Probe Metrics
  * `probe_duration_seconds`: How many seconds the backend probe took.
  * `probe_success`: Whether the probe was overall successful (0 or 1).
  * `probe_http_status_code`: The backend's HTTP status code.
  * `probe_http_content_length`: The length in bytes of the backend HTTP response content.
  * `probe_http_redirects`: How many redirects were followed.
  * `probe_http_ssl`: Whether the probe used SSL in the final redirect (0 or 1).
  * `probe_ssl_earliest_cert_expiry`: In the case of an SSL request, the Unix timestamp of the certificate expiry.
  * `probe_ip_protocol`: The IP protocol version used for the backend probe (4 or 6).
* `(probe_ssl_earliest_cert_expiry - time()) / 86400` to check how many days the SSL certificate will expire
  * `(probe_ssl_earliest_cert_expiry - time()) / 86400 < 30` use this for alert SSL certificate expire

### Relabeling steps

1. Before relabeling, the target has the following raw labels

```yaml
__address__: "https://prometheus.io"
__metrics_path__: "/probe"
__param_module: "http_2xx"
__scheme__: "http"
job: "blackbox"
```

2. The first relabeling rule adds a `__param_target` label, so the Blackbox exporter knows which target to scrape

```yaml
__address__: "https://prometheus.io"
__metrics_path__: "/probe"
__param_module: "http_2xx"
__param_target: "https://prometheus.io" # <=== Added.
__scheme__: "http"
job: "blackbox"
```

3. The second relabeling rule sets `instance` to the original `__address__` label's `https://prometheus.io` value so the target ends up labeled as expected for the user

```yaml
__address__: "https://prometheus.io"
__metrics_path__: "/probe"
__param_module: "http_2xx"
__param_target: "https://prometheus.io"
__scheme__: "http"
job: "blackbox"
instance: "https://prometheus.io" # <=== Added.
```

4. The third relabeling rule makes sure that we don't actually scrape `https://prometheus.io` itself, but the Blackbox Exporter's IP

```yaml
__address__: "127.0.0.1:9115" # <=== Overridden scrape address.
__metrics_path__: "/probe"
__param_module: "http_2xx"
__param_target: "https://prometheus.io"
__scheme__: "http"
job: "blackbox"
instance: "https://prometheus.io"
```

5. Finally, after relabeling, the user-visible labels of the target will simply be

```yaml
job: "blackbox"
instance: "https://prometheus.io"
```

## Navigation
* Previous: [Prometheus Alertmanager](03-prom-alert.md)
* [Home](../README.md)
* Next: [Grafana Basic](05-grafana-basic.md)
