# Grafana Loki

### Create Datasource
* Click toggle menu -> Conections -> Add new connection
* Search `loki` and click select `Loki` -> Add new data source
* http -> URL Input -> `http://loki:3100`
* Click `Save & test`
*

### Filter Data with Builder
* Click toggle menu -> Explore -> Tops Datasource -> select `Loki`
* Query A
* Label filter -> select label -> select `container` -> value `flog-error` -> click Run query
* Click `+ Operations` -> Line filter -> Line contain -> value `firewall`
* Label filter -> click `+` -> hostname -> select hostname self
* Click `+ Operations`-> Line filter -> Line does not contain -> value `XSS`
* Click `+ Operations`-> Aggregation -> Sum -> select SUM -> Click `+ By label`

* click `+ Add query`
* query B
* Label filter -> select label -> select `container` -> value `flog-combined` -> click Run query
* Click `+ Operations` -> Line filter -> Line contain -> value `500`

* remove all

### Filter Data with Code
* Query A -> click `Code`
* `{container="flog-error"} `
* `{container="flog-error"} |~ "XSS" `
* `{container="flog-error"} |~ "XSS"  !~"firewall"`
* `rate({container="flog-error"} |~ "XSS" !~"firewall" [$__interval])`
* `rate({container="flog-error"} |~ "XSS" !~"firewall" [$__interval]) + 2`

* `{container="prometheus"} | logfmt | filename="/etc/prometheus/prometheus.yml"`
* `{container="prometheus"} |~"Loading configuration"  | logfmt | filename="/etc/prometheus/prometheus.yml"`

### Filter On MOD DEV JSON
* Query A -> click `Code`
* `{app="order-management-service"}`
* `{app="order-management-service"}  | json  | body_teamId=~"<TEAM_ID>"` replace TEAM_ID
* `{app="order-management-service"}  | json  | body_teamId=~"<TEAM_ID>" | body_page="1"` replace TEAM_ID
* `{app="order-management-service"}  | json  | body_teamId=~"<TEAM_ID>" | body_limit="10"  | body_page="2"` replace TEAM_ID
* `sum(rate({app="order-management-service"} | json | body_teamId =~"<TEAM_ID>" | body_limit="10" [$__interval]))` replace TEAM_ID

## Navigation
* Previous: [Grafana Provision](07-grafana-provision.md)
* [Home](../README.md)
* Next: [Grafana Tempo](09-grafana-tempo.md)
