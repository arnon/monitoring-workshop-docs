# Monitoring Workshop

- Grafana
- Prometheus
- Alert Manager
- Loki
- Tempo


## Editor tools
- Docker compose
- [Cloud Shell](https://ssh.cloud.google.com)

## Ans WorkShop Code
[Ans](https://docs.google.com/document/d/1pCvwaPgaciaLqmxxDfjFn6B0BUZ5a__RvvrLi3jmKb0/edit)

# Documentation
- [01-Prepare](docs/01-prepare.md)
- [02-Prom-Basic](docs/02-prom-basic.md)
- [03-Prom-Alert](docs/03-prom-alert.md)
- [04-Prom-Exporter](docs/04-prom-exporter.md)
- [05-Grafana-Basic](docs/05-grafana-basic.md)
- [06-Grafana-Advance](docs/06-grafana-advance.md)
- [07-Grafana-Provision](docs/07-grafana-provision.md)
- [08-Grafana-Loki](docs/08-grafana-loki.md)
- [09-Grafana-Tempo](docs/09-grafana-tempo.md)